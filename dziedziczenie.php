<?php
// --- klasa bazowa
class Konto
{
private $numer;
function __construct($s)
{
$this->numer = $s;
}
public function setNumer($wartosc)
{
$this->numer = $wartosc;
}
public function getNumer()
{
return $this->numer;
}
}
// --- Klasa potomna
class Klient extends Konto
{
private $ime;
private $nazwisko;
function __construct($i,$n)
{
$this->imie = $i;
$this->nazwisko = $n;
}
public function setImie($wartosc)
{
$this->imie = $wartosc;
}
public function getImie()
{
return $this->imie;
}
public function setNazwisko($wartosc)
{
$this->nazwisko = $wartosc;
}
public function getNazwisko()
{
return $this->nazwisko;
}
}
$konto1 = new Klient('Jan','Kowalski');
$konto1->setNumer('23 123 0000 445634 33');
echo 'Numer konta: '.$konto1->getNumer();
echo "<br>";
echo 'Imię właściciela: '.$konto1->getImie()."<br>";
echo 'Nazwisko właściciela: '.$konto1->getNazwisko()."<br>";
?>
<!-- 
Zdefiniowaliśmy klasę Konto, 
która posiada jedno pole prywatne - numer konta (numer). 
Klasa Konto posiada konstruktor i metody odczytu i zapisu 
wartości pola.
Tworzymy drugą klasę Klient, 
która posiada pola imię i nazwisko klienta (imie, nazwisko). 
Ta klasa również posiada konstruktor i zdefiniowane metody odczytu i zapisu wartości obu pól. 
Ponieważ użyliśmy słowa kluczowego extends to klasa Klient rozszerza klasę Konto.
Teraz tworzymy obiekt w klasie Klient z konkretnymi danymi osobowymi i numerem konta.
Robimy to w klasie Klient, a nie w klasie Konto. 
Metoda setNumer() (pokaż numer) jest dostępna w obu klasach, mimo, że w klasie Klient jej nie definiujemy.
Metoda ta została zdefiniowana w klasie Konto. 
Dzieki "dziedziczeniu" mamy w klasie potomnej dostęp do metod i pól obydwuch klas. 
 -->
