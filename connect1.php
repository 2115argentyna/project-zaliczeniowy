<html>
   <head>
      <title>Create a MariaDB Database</title>
   </head>

   <body>
      <?php
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
      
         if(! $conn ) {
            die('Could not connect: ' . mysql_error());
         }

         echo 'Connected successfully <hr>';
         $sql = 'CREATE DATABASE PRODUCTS';
         $retval = mysqli_query( $conn, $sql );
      
         if(! $retval ) {
            die('Could not create database: ' . mysql_error());
         }

         echo "Database PRODUCTS created successfully\n<hr>";
         mysqli_close($conn);
      ?>
   </body>
</html>