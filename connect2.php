<<html>
   <head>
      <title>Create a MariaDB Table</title>
   </head>

   <body>
      <?php
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
      
         if(! $conn ){
            die('Could not connect: ' . mysqli_error());
         }
         echo 'Connected successfully<br />';
         
         $sql = "CREATE TABLE products from products".
            "product_id INT NOT NULL AUTO_INCREMENT, ".
            "product_name VARCHAR(100) NOT NULL, ".
            "product_manufacturer VARCHAR(40) NOT NULL, ".
            "submission_date DATE, ".
            "PRIMARY KEY ( product_id )); ";
      
         mysqli_select_db( PRODUCTS );
         $retval = mysqli_query( $conn, $sql, );
      
         if(! $retval ) {
            die('Could not create table:' . mysqli_error());
         }
         echo "Table created successfully\n";
         
         mysqli_close($conn);
      ?>
   </body>
</html>